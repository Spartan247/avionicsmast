#ifndef LOWPASS
#define LOWPASS
#include <math.h>
#include <Arduino.h>
class LowPass {
public:
	double cutoff;
	double val;
	bool initialized=false;
	double lastsample;
	double p;//milliseconds
	LowPass(double cutoff) {
		this->cutoff=cutoff;
	}
	double update(double in) {
		if(isnan(in)) return 0;
		if(!initialized) {
			initialized=true;
			lastsample=millis();
			val=in;
			return val;
		}
		double beta = exp(-0.001*cutoff*(millis()-lastsample));
		val=beta*val+(1-beta)*in;
		lastsample=millis();
		return val;
	}
	void setCutoff(double cutoff) {
		this->cutoff=cutoff;
	}
};
#endif
