#ifndef AOAWIFI
#define AOAWIFI
#include <Arduino.h>
#include <WiFi.h>
#include <SPI.h>
#include <SD.h>
#include <cstring>
//char ssid[] = "AoA";        // your network SSID (name)
//char pass[] = "aoatesting";    // your network password (use for WPA, or use as key for WEP)
int keyIndex = 0;

int status = WL_IDLE_STATUS;
WiFiServer server(80);

IPAddress local_IP(192,168,4,22);
IPAddress gateway(192,168,4,9);
IPAddress subnet(255,255,255,0);

void wifisetup() {
  	Serial.println("Scanning");
  	WiFi.scanNetworks();
  	while(WiFi.scanComplete()<0);
  	auto num=WiFi.scanComplete();
  	String SSID;
  	String pass;
  	for(int i = 0; i<num;i++) {
  	  Serial.println(WiFi.SSID(i));
  	  if(WiFi.SSID(i).equals("millsgap")) {SSID="millsgap";pass="62946295";}
  	  if(WiFi.SSID(i).equals("stratux")) {SSID="stratux";break;}
  	}
	  Serial.println("Connecting to "+SSID);
  	WiFi.begin(SSID.c_str(),pass.c_str());
	  for(int i = 0;i<10||WiFi.status()!=WL_CONNECTED;i++) delay(500);
	if(WiFi.status()!=WL_CONNECTED) ESP.restart();  
  server.begin();
  Serial.printf("Web server started, open %s in a web browser\n", WiFi.localIP().toString().c_str());
}
String instrument = "{}";
void wifiloop(String in) {
	if(WiFi.status()!=WL_CONNECTED) ESP.restart();
  instrument = in;
  // compare the previous status to the current status
  if (status != WiFi.status()) {
    // it has changed update the variable
    status = WiFi.status();

    if (status == WL_CONNECTED) {
      // a device has connected to the AP
      Serial.println("Device connected to AP");
    } else {
      // a device has disconnected from the AP, and we are back in listening mode
      Serial.println("Device disconnected from AP");
    }
  }

  WiFiClient client = server.available();   // listen for incoming clients

  if (client) {                             // if you get a client,
    Serial.println("new client");           // print a message out the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        if (c == '\n') {                    // if the byte is a newline character
        
        if(strstr(currentLine.c_str(),"GET /")!=nullptr) {
					String filepath = "PAGES/";
					filepath.concat(strstr(currentLine.c_str(),"GET /")+5);
					filepath=filepath.substring(0,filepath.indexOf(' '));
					Serial.println("Filepath: " + filepath);
					if(SD.exists(filepath)){
						Serial.println("File exists");
	          client.println("HTTP/1.1 200 OK");
	          client.println("Content-type:text/html");
	          //client.println("Access-Control-Allow-Origin: *");
	          client.println();
	          File file = SD.open(filepath,FILE_READ);
	          while(file.available()) client.print(file.read());
	          client.println();
          }
        }

          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {

            // The HTTP response ends with another blank line:
            client.println();
            // break out of the while loop:
            break;
          }
          else {      // if you got a newline, then clear currentLine:
            currentLine = "";
          }
        }
        else if (c != '\r') {    // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
   				//currentLine.concat(client.readStringUntil('\n'));
        }
        // Check to see if the client request was "GET /H" or "GET /L":

        if (currentLine.endsWith("GET /data")) {
          client.println("HTTP/1.1 200 OK");
          client.println("Content-type:text/html");
          client.println("Access-Control-Allow-Origin: *");
          client.println();
          client.println(instrument);
          client.println();
        }
        if (currentLine.endsWith("GET /display")) {
          client.println("HTTP/1.1 200 OK");
          client.println("Content-type:text/html");
          client.println("Access-Control-Allow-Origin: *");
          client.println();
          client.println("<html> <head> </head> <body> <script type=\"text/javascript\">function doAPIcall(type, url, callback){var xmlhttp=new XMLHttpRequest(); xmlhttp.onreadystatechange=function (){if (xmlhttp.readyState==XMLHttpRequest.DONE && xmlhttp.status==200){var data=xmlhttp.responseText; if (callback) callback(data);}}; xmlhttp.open(type, url, true); xmlhttp.send();}setInterval(function(){doAPIcall( \"GET\", \"data\", function (data){var d=JSON.parse(data); document.getElementById(\"pressureAltitude\").innerText=calcAltitude(29.92,d.Ps); document.getElementById(\"altitude\").innerText=calcAltitude(document.getElementById(\"altimeter\").value,d.Ps); document.getElementById(\"aoa\").innerText=d.aoa; document.getElementById(\"airspeed\").innerText=d.airspeed;});},500); function calcAltitude(altimeterSetting, Ps){setting=altimeterSetting * 3386.39; console.log(\'setting in pa\', setting); var g=9.81; var b=288.15; var m=-.0065; var R=287; var h=(b/m)*((Ps/setting)**(-m*R/g)-1); return Math.round(h*3.281,0);}var frequency; var duration; var dutyCycle; var gain; var audioEnabled=false; var audioCtx=new(window.AudioContext || window.webkitAudioContext)(); function playNote(){if(audioEnabled){var aoa=parseFloat(document.getElementById(\"aoa\").innerText); if(parseFloat(document.getElementById(\"airspeed\").innerText)<20) aoa=0; if(aoa<9.5){frequency=700; duration=1000; dutyCycle=.05; gain=.2;}if(aoa>=9.5 && aoa<10.5){frequency=700; duration=500; dutyCycle=.8; gain=.2;}if(aoa>=10.5 && aoa<11.5){frequency=600; duration=1000; dutyCycle=1.0; gain=.4;}if(aoa>=11.5 && aoa<12.5){frequency=800; duration=500; dutyCycle=.8; gain=.8;}if(aoa>=12.5){frequency=800; duration=300; dutyCycle=.8; gain=1;}console.log(\'settings:\',frequency, duration, dutyCycle); var volume=audioCtx.createGain(); volume.connect(audioCtx.destination); volume.gain.value=gain; var oscillator=audioCtx.createOscillator(); oscillator.type=\'square\'; oscillator.frequency.value=frequency; oscillator.connect(volume); oscillator.start(); setTimeout( function(){oscillator.stop(); setTimeout( function(){console.log(\'wait\'); playNote();}, Math.round(duration*(1-dutyCycle),0));}, Math.round(duration*dutyCycle,0));}}</script> Altimeter Setting:<input type=\"text\" id=\"altimeter\" value=\"29.92\"/><br/> Pressure Altitude:<label id=\"pressureAltitude\"></label><br/> Altitude:<label id=\"altitude\"></label><br/> Airspeed:<label id=\"airspeed\"></label><br/> AOA:<label id=\"aoa\"></label><input type=\"button\" value=\"toggle audio\" onclick=\"javascript:audioEnabled=!audioEnabled; playNote();\"/> </body></html>");
          client.println();
        }
      }
    }
    // close the connection:
    client.stop();
    Serial.println("client disconnected");
  }
}
#endif
