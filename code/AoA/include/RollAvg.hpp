#ifndef ROLLAVG
#define ROLLAVG
#include <math.h>
#include <Arduino.h>
#include <stdint.h>
class RollingAverage {
	public:
	double buf[50];
	uint8_t it;
	unsigned long p;
	RollingAverage() {
		it=0;
	}
	double avg() {
		int total = 0;
		long double sum = 0;
		for(int i = 0; i<50;i++) {
			if(!isnan(buf[i])) {
				total++;
				sum+=buf[i];
			}
		}
		return sum/total;
	}
	double update(double in) {
		buf[it]=in;
		it++;
		it%=50;
		return avg();
	}

};
#endif
