#ifndef MS4525DO_H
#define MS4525DO_H
#include <Arduino.h>
#include <Wire.h>
#include "TwoWireAbs.hpp"

class MS4525DO {//For output type A only for now
private:
  int interrupt;
  int8_t addr;
  double pmax,pmin;
  double pressure;
  double temperature;
  TwoWireAbs *i2c;
public:
  MS4525DO(TwoWireAbs &i2cPort,int interruptpin=-1,double maxPressure=1,double minPressure=-1,uint8_t address=0x28) {
    pmax=maxPressure; pmin=minPressure; addr=address; interrupt = interruptpin; i2c=&i2cPort;
  }
  void poll() {
    if(interrupt>-1) {
      while(digitalRead(interrupt)==LOW);
    }
    i2c->requestFrom(addr, 4, true);
    uint16_t pres=i2c->read();
    pres<<=8;
    pres+=i2c->read();
    uint16_t temp = i2c->read();
    temp<<=8;
    temp+=i2c->read();
    temp>>=5;
    pressure=pmin+(((double)pres-0.1*16383)*(pmax-pmin))/(0.8*16383);
    temperature=-50+(double)temp*0.0977039570103;
  }
  void getData(double &Pressure, double &Temperature) {poll(); Pressure=pressure;Temperature=temperature;}
  double getPressure() {poll();return pressure;}
  double getTemperature() {poll();return temperature;}
};

#endif
