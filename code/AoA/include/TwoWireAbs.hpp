#ifndef TWOWIREABS
#define TWOWIREABS
#include <Arduino.h>

class TwoWireAbs {
  private:

  public:
    virtual void begin(){}
    virtual void begin(int8_t address){}
    virtual void end(){}
    virtual byte requestFrom(uint8_t address, uint8_t quantity, uint8_t stop = true){return 0;}
    virtual void beginTransmission(int8_t address){}
    virtual int8_t endTransmission(bool stop){return 0;}
    virtual size_t write(byte value){return 0;}
    virtual size_t write(char* string){return 0;}
    virtual size_t write(byte *data,unsigned int length){return 0;}
    virtual int available(){return 0;}
    virtual int read(){return 0;}
};

#endif
