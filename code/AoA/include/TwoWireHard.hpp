#ifndef TWOWIREHARD
#define TWOWIREHARD
#include <Arduino.h>
#include <Wire.h>
#include "TwoWireAbs.hpp"

class TwoWireHard : public TwoWireAbs {
	private:
		TwoWire *i2c;
	public:
		TwoWireHard() {
			i2c=&Wire;
		}
		TwoWireHard(TwoWire &wire) {
			i2c=&wire;
		}
		void begin() {i2c->begin();}
		void begin(int8_t address) {i2c->begin(address);}
		void end() {i2c->end();}
		byte requestFrom(uint8_t address, uint8_t quantity, uint8_t stop = true) {return i2c->requestFrom( address,   quantity,  stop );}
		void beginTransmission(int8_t address) {i2c->beginTransmission(address);}
		int8_t endTransmission(bool stop) {return i2c->endTransmission(stop);}
		size_t write(byte value) {return i2c->write(value);}
		size_t write(char *string) {return i2c->write(string);}
		size_t write(byte *data,unsigned int length) {return i2c->write(data,length);}
		int available() {return i2c->available();}
		int read() {return i2c->read();}
};

#endif
