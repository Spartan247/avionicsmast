#ifndef TWOWIRESOFT
#define TWOWIRESOFT
#include <Arduino.h>
#include <SoftWire.h>
#include "TwoWireAbs.hpp"

class TwoWireSoft : public TwoWireAbs {
private:
  SoftWire *i2c;
public:
  TwoWireSoft(SoftWire &wire) {
    i2c=&wire;
  }
  void begin() {i2c->begin();}
  void end() {i2c->end();}
  byte requestFrom(uint8_t address, uint8_t quantity, uint8_t stop = true) {return i2c->requestFrom( address,   quantity,  stop );}
  void beginTransmission(int8_t address) {i2c->beginTransmission(address);}
  int8_t endTransmission(bool stop) {return i2c->endTransmission(stop);}
  size_t write(byte value) {return i2c->write(value);}
  size_t write(char *string) {return i2c->Print::write(string);}
  size_t write(byte *data,unsigned int length) {return i2c->write(data,length);}
  int available() {return i2c->available();}
  int read() {return i2c->read();}
};

#endif
