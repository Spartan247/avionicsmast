#ifndef RATELIMITER
#define RATELIMITER
#include <math.h>
#include <Arduino.h>
class RateLimiter {
	public:
	unsigned long last;
	unsigned int period;
	RateLimiter(unsigned int period_ms) {
		period=period_ms;
		last=millis();
	}
	void update() {
		while(millis()-last<period);
		last=millis();
	}
};
#endif
