#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BMP3XX.h"
#include <Adafruit_MPU6050.h>
//#include <EEPROM.h>
#include <math.h>
#include "LowPass.hpp"
#include "RollAvg.hpp"
#include "Ratelimiter.hpp"
#include "MS4525DO.hpp"
#include "TwoWireHard.hpp"
#include "TwoWireSoft.hpp"
#include <SoftWire.h>
#include <string.h>
#include "aoaWifi.hpp"
#include "RTClib.h"
RTC_PCF8523 rtc;

TwoWireHard wire;
SoftWire sw(14,32);
char txbuf[50],rxbuf[50];
TwoWireSoft wire2(sw);
MS4525DO sense(wire);
MS4525DO sense2(wire2);
Adafruit_BMP3XX bmp;
Adafruit_MPU6050 mpu;

#define DRIFT 0.002
double rho = 1.225; //Density at MSL
double grav = 9.81; //Accleration due to gravity
double Tmsl = 288.16; //Temperature at MSL, in absolute units
double Tlapse = -6.5/1000; //Temperature lapse rate
double R = 287; //Specific gas constant
double Pmsl = 1.01325*pow(10,5); //Pressure at MSL, or altimeter setting

float MPUoffset[] = {0.23,0.03,-2.61};
float MPUfactor[]= {9.51,9.68,10.13};

File file;
bool sderr=false,bmperr=false,imuerr=false;

void setup() {
  Serial.begin(115200);
  while(!Serial);
  delay(1000);
  Serial.println("Starting");
  wifisetup();
  Wire.begin();
  sw.setTxBuffer(txbuf, sizeof(txbuf));
  sw.setRxBuffer(rxbuf, sizeof(rxbuf));
  sw.begin();
  pinMode(LED_BUILTIN,OUTPUT);
  Serial.println("I2C Started");
  
  int tries=100;
  while(!bmp.begin_I2C() && tries>0) {Serial.println("Starting BMP");tries--;}
  if(tries>0) Serial.println("BMP Started");
  else {bmperr=true; Serial.println("BMP Failed");}
  
  Serial.println("Starting SD");
  tries=50;
  while(!SD.begin()&&tries>0) {Serial.println("Starting SD");tries--;}
  if(tries>0) {
		Serial.println("Checking for log folder");
		if(!SD.exists("/LOGS/")) SD.mkdir("/LOGS");
		int i;
		Serial.println("Checking for log files");
		for(i = 0;SD.exists("/LOGS/LOG"+String(i));i++);
		Serial.println("Opening: /LOGS/LOG"+String(i));
		file=SD.open("/LOGS/LOG"+String(i), FILE_WRITE);
		rtc.begin();
	} else {sderr=true;Serial.println("SD Card Failed");}

  Serial.println("Starting IMU");
  tries = 500;
  while(!mpu.begin(0x69) && tries>0) tries--;
  if(tries>0) Serial.println("IMU Started");
  else {imuerr=true; Serial.println("IMU Failed");}
}

LowPass Psf(2);
LowPass q1f(2);
LowPass q2f(2);
RateLimiter limiter(100);
//RollingAverage Psf;
//RollingAverage P01f;
//RollingAverage P02f;

auto lastpoll=millis();

void dtos(double in, char* buf) {
  sprintf(buf,"%d.%d",(int)in,((int)abs(in*100))%100);
}

void loop() {
  //limiter.update();
  double Ps;
  double rawPs=0;
  if(bmperr) Ps=0;
  else {
		bmp.performReading();
		double rawPs=bmp.pressure;
		Ps=Psf.update(rawPs);
	}
	
  double rawq1=sense.getPressure()*6894.757;
  double q1=q1f.update(rawq1);

  double rawq2=sense2.getPressure()*6894.757;
  double q2=q2f.update(rawq2);

  double altitude = (Tmsl/Tlapse)*(pow((Ps)/Pmsl,-Tlapse*R/grav)-1);
  double airspeed = sqrt(2*(q1)/rho);
  if(isnan(airspeed)) airspeed=0;
  double maxair=sqrt(2/rho)*sqrt(q1+pow((sqrt(q2)-cos(60.0*M_PI/180.0)*sqrt(q1))/(sin(60.0*M_PI/180.0)),2));
  if(isnan(maxair)) maxair=0;
  double aoa=180*atan(-1*(sqrt(3)*(sqrt(q1)-2*sqrt(q2)))/(3*sqrt(q1)))/M_PI;
  if(isnan(aoa)) aoa=0;
	
	double y;
	double pitch;
	if(imuerr) pitch=0;
	else {
		sensors_event_t events[3];
		mpu.getEvent(events,events+1,events+2);
		double x=(events[0].acceleration.x-MPUoffset[0])/MPUfactor[0];
		y=(events[0].acceleration.y-MPUoffset[1])/MPUfactor[1];
		pitch = atan2(x,-1*y)*180/M_PI;
	}
	
  bool warning = false;
  if(true) {
    char string[500]="test";
    char airstr[20];
    char aoastr[20];
    char pitchstr[20];
    char gstr[20];
    dtos(1.943844*airspeed,airstr);
    dtos(aoa,aoastr);
    dtos(pitch,pitchstr);
    dtos(-1.0*y,gstr);
    sprintf(string, "{\"altitude\":%d,\n\r\"airspeed\":%s,\n\r\"aoa\":%s,\n\r\"time\":%lu,\n\r\"Ps\":%ld,\n\r\"q1\":%ld,\n\r\"q2\":%ld,\n\r\"rawPS\":%ld,\n\r\"rawq1\":%ld,\n\r\"rawq2\":%ld,\n\r\"pitch\":%s,\n\r\"sderr\":%d,\n\r\"bmperr\":%d,\n\r\"imuerr\":%d,\n\r\"g\":%s}",(int)round(3.280839895*(altitude)),airstr,aoastr,(rtc.now().unixtime()),(long)round(Ps),(long)round(q1),(long)round(q2),(long)round(rawPs),(long)round(rawq1),(long)round(rawq2),pitchstr,sderr,bmperr,imuerr,gstr);
		if(!sderr) {
		  file.println(string);
		  file.flush();
    }
    //Serial.print(string);
    wifiloop(string);
    //Serial.println(String(Ps)+","+String(P01)+","+String(P02));
  }
  lastpoll=millis();
}
