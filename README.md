The goal of this project is to be a self contained avionics system providing backups for the six pack, as well as pressure-based angle of attack.

Currently the project consists of the mast with necessary pressure ports connected to a separate board with the pressure sensors for airspeed and angle of attack. The altimiter sensor is embedded into the mast.

To build:
pio run

To upload:
pio run -t upload

driver for mac: http://www.wch-ic.com/downloads/CH34XSER_MAC_ZIP.html
